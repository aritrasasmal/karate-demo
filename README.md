# karate-demo

  
  
  

## Getting Started 
Refer to this [page](https://github.com/karatelabs/karate/wiki/IDE-Support) for enabling IDE support before/after cloning the project.

You can clone the project by running in your terminal:

```
git clone https://gitlab.com/aritrasasmal/karate-demo.git
```

## Usage
You can directly run any of the following classes from your IDE, which will run all the scenarios in each feature files while ignoring the ones which have been tagged by @*ignore* tag:

|Runner Class|Usage|
|-|-|
|KarateTestRunner|Will execute all the features sequentially in a single thread|
|ParallelTestRunner|Will execute all the features parallely across multiple threads| 

Alternatively, you can also achieve the same using the terminal:
```
cd karate-demo
mvn clean test -Dtest=KarateTestRunner
mvn clean test -Dtest=ParallelTestRunner
```
Now, if you want to run the tests (be it feature level or scenario level) specific to some tag, let's say @*scenario1*, you need run:

```
mvn clean test -Dtest=KarateTestRunner -Dkarate.options="--tags @scenario1"
```
>Or,
```
mvn clean test -Dtest=ParallelTestRunner -Dkarate.options="--tags @scenario1"
```

Similarly, if you want to run all the tests excluding the ones specific to some tag, let's say @*scenario2*, you need run:

```
mvn clean test -Dtest=KarateTestRunner -Dkarate.options="--tags ~@scenario2"
```
>Or,
```
mvn clean test -Dtest=ParallelTestRunner -Dkarate.options="--tags ~@scenario2"
```
Finally, if you want to run the tests specific to and environment, let's say *mob2*, you need to run:
```
mvn clean test -Dtest=KarateTestRunner -Dkarate.env=mob2
```
> And if with specific tag,
```
mvn clean test -Dtest=KarateTestRunner -Dkarate.env=mob2 -Dkarate.options="--tags @scenario1
```
## Reporting
At the end of test execution a parallel stat will be published if multiple features are executed or if **ParallelTestRunner** is executed
```
======================================================
elapsed:   1.99 | threads:    5 | thread time: 1.10 
features:     1 | skipped:    4 | efficiency: 0.11
scenarios:    2 | passed:     2 | failed: 0
======================================================
```
And a HTML report is generated:
```
HTML report: (paste into browser to view) | Karate version: 1.1.0
file:///Users/aritrasasmal/Documents/Code/karate-demo/target/karate-reports/karate-summary.html
===================================================================
```
