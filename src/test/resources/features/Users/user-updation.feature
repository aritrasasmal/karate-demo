Feature: sample karate test script to update user

  Background:
    * url baseUrl
    * def updateUserPayload = read('classpath:requests/user/update_user.json')

  Scenario: update user by id
    Given path 'users', '1'
    * set updateUserPayload.name = 'John'
    * set updateUserPayload.address.zipcode = '712-202'
    And request updateUserPayload
    When method put
    Then status 200
    * match response.name == updateUserPayload.name
    * match response.address.zipcode == updateUserPayload.address.zipcode

  Scenario: patch user by id
    Given path 'users', '1'
    * def payload =
    """
    {
      "name": "James",
      "address": {
        "zipcode": "560-075"
      }
    }
    """
    And request payload
    When method patch
    Then status 200
    * match response.name == payload.name
    * match response.address.zipcode == payload.address.zipcode
