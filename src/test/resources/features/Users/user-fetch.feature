Feature: sample karate test script to get users
  for help, see: https://github.com/intuit/karate/wiki/IDE-Support

  Background:
    * url baseUrl

  @regression
  Scenario: get all users
    Given path 'users'
    When method get
    Then status 200


  @regression
  Scenario: get user by id
    Given path 'users', '1'
    When method get
    Then status 200