Feature: sample karate test script to create users
  for help, see: https://github.com/intuit/karate/wiki/IDE-Support

  Background:
    * url baseUrl
    * def createUserPayload = read('classpath:requests/user/create_user.json')
    * def responseSchema = read('classpath:response_schema/user/create_user.json')

  Scenario: create a user
    Given path 'users'
    And request createUserPayload
    When method post
    Then status 201
    And match response == responseSchema
    And match response.name == createUserPayload.name

  Scenario: create another user
    Given path 'users'
    * set createUserPayload.name = 'John'
    And request createUserPayload
    When method post
    Then status 201
    And match response == responseSchema
    And match response.name == createUserPayload.name