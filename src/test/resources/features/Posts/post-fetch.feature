Feature: sample karate test script to get posts
  for help, see: https://github.com/intuit/karate/wiki/IDE-Support

  Background:
    * url baseUrl

  @scenario1
  Scenario: get all posts
    Given path 'posts'
    When method get
    Then status 200


  @scenario2
  Scenario: get post by id
    Given path 'posts', '1'
    When method get
    Then status 200