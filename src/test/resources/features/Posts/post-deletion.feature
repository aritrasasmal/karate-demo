Feature: sample karate test script to delete post
  for help, see: https://github.com/intuit/karate/wiki/IDE-Support

  Background:
    * url baseUrl

  @scenario1
  Scenario: delete post by id
    Given path 'posts', '1'
    When method delete
    Then status 200