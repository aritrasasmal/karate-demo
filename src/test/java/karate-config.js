function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'mob1';
  }
  var config = {
    env: env,
    baseUrl: 'https://jsonplaceholder.typicode.com', // mob 1 url
    token: 'mob1 token'
  }
  if (env == 'mob2') {
    // customize
    // e.g. config.foo = 'bar';
    config.baseUrl = 'mob2 url'
  } else if (env == 'prod') {
    // customize
    config.baseUrl = 'prod url'
  }
  return config;
}