package com.myorg.testrunners;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParallelTestRunner {
    private static final int THREAD_COUNT = 5;

    @Test
    void test() {
        Results results = Runner.path("classpath:features").tags("~@ignore")
                .parallel(THREAD_COUNT);
        assertEquals(0, results.getFailCount(), results.getErrorMessages());
    }

}
