package com.myorg.testrunners;

import com.intuit.karate.junit5.Karate;

class KarateTestRunner {

    @Karate.Test
    Karate testFeatures() {
        return Karate.run("classpath:features").tags("~@ignore");
    }

}
